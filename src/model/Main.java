package model;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Book[] books = Generator.generate();
        Library library = new Library(Generator.generate());
        // print old book;
        library.getOldestBook().print();
        // find book by author
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите автора книги");
        String author = scan.nextLine();
        library.printBookWithAutor(author);
        // find book older then value
        System.out.println("Введите год издания книги: ");
        int year = Integer.parseInt(scan.nextLine());
        library.printBooksOlderThen(year);
        // find book by many author
        System.out.println("Введите автора книги: ");
        String author1 = scan.nextLine();
        library.printBookWithManyAutors(author1);
        // find book 3 and many author
        System.out.println("Книги с тремя авторами и более: ");
        library.printBookWith3Autors(author1);
    }
}


