package model;

public class Library {
    Book[] books;

    public Library(Book[] books) {
        this.books = books;
    }

    public Book getOldestBook() {
        // find old book
        Book oldestBook = books[0];
        for (int i = 1; i < books.length; i++) {
            oldestBook = oldestBook.compareAndReturnOldest(books[i]);
        }
        return oldestBook;
    }

    public void printBookWithAutor(String author) {
        boolean isBookFinds = false;
        for (Book book : books) {
            if (book.hasAuthor(author)) {
                book.print();
                isBookFinds = true;
            }
        }
        if (!isBookFinds) {
            System.out.println("---- такого автора нет");
        }
    }

    public void printBookWithManyAutors(String author1) {
        boolean isBookFinds = false;
        for (Book book : books) {
            if (book.hasManyAuthor(author1)) {
                book.print();
                isBookFinds = true;
            }
        }
        if (!isBookFinds) {
            System.out.println("Такого автора нет");
        }
    }

    public void printBookWith3Autors(String author3) {
        boolean isBookFinds = false;
        for (Book book : books) {
            if (book.has3Author(author3)) {
                book.print();
                isBookFinds = true;
            }
        }
        if (!isBookFinds) {
            System.out.println("Такого автора нет");
        }
    }

    public void printBooksOlderThen(int year) {
        for (Book book : books) {
            if (book.isOlderThen(year)) {
                book.print();
            }
        }

    }
}



