package model;

public class Book {
    public String nameBook;
    public String authorBook;
    public int yearBook;
    public String izdatelstvoBook;

    public Book(String nameBook, String authorBook, int yearBook, String izdatelstvoBook) {
        this.nameBook = nameBook;
        this.authorBook = authorBook;
        this.yearBook = yearBook;
        this.izdatelstvoBook = izdatelstvoBook;
    }

    public void print() {
        System.out.println("Название: " + nameBook);
        System.out.println("Авторство: " + authorBook);
        System.out.println("Год: " + yearBook);
        System.out.println("Издательство: " + izdatelstvoBook);
    }

    public boolean isOlderThen(Book book) {
        return yearBook > book.yearBook;
    }

    public boolean isOlderThen(int yearBook) {
        return this.yearBook < yearBook;
    }

    public Book compareAndReturnOldest(Book book) {
        return yearBook < book.yearBook ? this : book;
    }

    public boolean hasAuthor(String nameBook) {
        return authorBook.equalsIgnoreCase(nameBook);
    }

    public boolean hasManyAuthor(String nameBook) {
        return authorBook.contains(nameBook);
    }

    public boolean has3Author(String nameBook) {
        return authorBook.contains("3");
    }
}




